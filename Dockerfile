FROM node:6-alpine

COPY . ./
RUN npm install

CMD ["npm", "start"]
EXPOSE $PORT
